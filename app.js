//Storage Controller
const StorageCtrl = (function () {
  return {
    getItemsListFromStorage: function () {
      let storageItems;

      if (localStorage.getItem("menuItems") === null) {
        storageItems = [];
      } else {
        storageItems = JSON.parse(localStorage.getItem("menuItems"));
      }

      return storageItems;
    },
    updateItemList: function (newItem) {
      // get current LS items
      const localStorageItems = this.getItemsListFromStorage();

      //update menu item
      localStorageItems.forEach((currentItem, index) => {
        if (currentItem.id === newItem.id) {
          localStorageItems.splice(index, 1, newItem);
        }
      });

      //update LS
      localStorage.setItem("menuItems", JSON.stringify(localStorageItems));
    },
    removeItemFromStorage: function (itemToDeleteId) {
      // retrieve items from storage
      const localStorageItems = StorageCtrl.getItemsListFromStorage();

      // remove items from temporary DS
      localStorageItems.forEach((currentItem, index) => {
        if (currentItem.id === itemToDeleteId) {
          localStorageItems.splice(index, 1);
        }
      });

      // Set/update LS
      localStorage.setItem("menuItems", JSON.stringify(localStorageItems));
    },
    saveItemToStorage: function (newItem) {
      let storageItems;
      // Retrieve current local storage
      if (localStorage.getItem("menuItems") === null) {
        storageItems = [];
      } else {
        storageItems = JSON.parse(localStorage.getItem("menuItems"));
      }
      // Add menu item to array
      storageItems.push(newItem);

      // Update/resave local storage
      localStorage.setItem("menuItems", JSON.stringify(storageItems));
    },
  };
})();

//Item Controller
const ItemCtrl = (function () {
  // Private Data
  // Item constructor
  const Item = function (id, name, calories) {
    this.id = id;
    this.name = name;
    this.calories = calories;
  };

  // Data Structure
  const data = {
    items: StorageCtrl.getItemsListFromStorage(),
    currentMeal: null,
    totalCalories: 0,
  };

  // Public Data

  return {
    createNewItem(newItemInput) {
      // Create id
      let ID;
      if (data.items.length > 0) {
        //   ds            item            id   1
        ID = data.items[data.items.length - 1].id + 1;
      } else {
        ID = 0;
      }

      // Create item
      const newItem = new Item(
        ID,
        newItemInput.mealName,
        parseInt(newItemInput.mealCalories)
      );

      // Add item to ds
      data.items.push(newItem);

      return newItem;
    },
    getAllData: function () {
      return data;
    },
    getCurrentMeal: function () {
      return data.currentMeal;
    },
    getItems: function () {
      return data.items;
    },
    getTotalCalories: function () {
      let currentTotal = 0;

      //Get current total
      data.items.forEach((item) => (currentTotal += item.calories));

      //Set total in DS
      data.totalCalories = currentTotal;

      return data.totalCalories;
    },
    getMealById: function (mealId) {
      let itemFound = null;
      data.items.forEach((currentItem) => {
        if (currentItem.id === mealId) {
          itemFound = currentItem;
        }
      });
      return itemFound;
    },
    setCurrentMeal: function (meal) {
      data.currentMeal = meal;
    },
    changeMealInfo: function (currentMealId, updatedName, updatedCalories) {
      const menuItems = ItemCtrl.getItems();
      menuItems[currentMealId].name = updatedName;
      menuItems[currentMealId].calories = updatedCalories;
    },
    updateItemList: function (newItemList) {
      data.items = newItemList;
    },
  };
})();

//UI Controller
const UiCtrl = (function (ItemCtrl) {
  // Private data
  // UI Selectors
  const uiElements = {
    addButton: ".add-btn",
    backButton: ".back-btn",
    clearBtn: ".clear-btn",
    editButton: ".fa fa-pencil",
    updateButton: ".update-btn",
    deleteButton: ".delete-btn",
    itemList: "#item-list",
    mealName: "#item-name",
    mealCalories: "#item-calories",
    totalCaloriesLabel: ".total-calories",
    cardContainer: ".card-content",
  };

  const createMealDomElement = function (item) {
    // create food list item
    const listItem = document.createElement("li");
    listItem.className = "collection-item";
    listItem.id = `item-${item.id}`;

    // populate data
    listItem.innerHTML = `
        <strong>${item.name}: </strong> <em>${item.calories} Calories</em>
            <a href="" class="secondary-content"><i class="edit-item fa fa-pencil"></i></a>
        </li>
        `;

    return listItem;
  };

  const displayMealList = function () {
    document.querySelector(uiElements.itemList).style.display = "block";
  };

  // Public functions

  return {
    clearMealList: function () {
      document.querySelector(uiElements.itemList).innerHTML = "";
    },
    renderSingleItemToUI: function (newItem) {
      // display meal list
      displayMealList();

      // create element and add to UI
      const mealDomElement = createMealDomElement(newItem);
      document.querySelector(uiElements.itemList).appendChild(mealDomElement);

      ItemCtrl.getTotalCalories();
    },
    clearEditState: function () {
      // Clear inputs
      this.clearFields();

      // Hide back, edit, and delete buttons
      document.querySelector(uiElements.backButton).style.display = "none";
      document.querySelector(uiElements.updateButton).style.display = "none";
      document.querySelector(uiElements.deleteButton).style.display = "none";

      // Show add button
      document.querySelector(uiElements.addButton).style.display = "inline";
    },
    clearFields: function () {
      document.querySelector(uiElements.mealName).value = "";
      document.querySelector(uiElements.mealCalories).value = "";

      document.querySelector(uiElements.mealName).focus();
    },
    getItemInput: function () {
      return {
        mealName: document.querySelector(uiElements.mealName).value,
        mealCalories: document.querySelector(uiElements.mealCalories).value,
      };
    },
    hideMealList: function () {
      document.querySelector(uiElements.itemList).style.display = "none";
    },
    renderItemsToUI: function (items) {
      items.forEach((item) => {
        // create food list item
        const listItem = createMealDomElement(item);

        // Add item to list
        document.querySelector(uiElements.itemList).appendChild(listItem);
      });
    },
    getUiSelectors: function () {
      return uiElements;
    },
    showEditMeal: function (meal) {
      // populate meal name and calories
      document.querySelector(uiElements.mealName).value = meal.name;
      document.querySelector(uiElements.mealCalories).value = meal.calories;

      // show edit buttons
      UiCtrl.showEditState();
    },
    showEditState: function () {
      // Show back, edit, and delete buttons
      document.querySelector(uiElements.backButton).style.display = "inline";
      document.querySelector(uiElements.updateButton).style.display = "inline";
      document.querySelector(uiElements.deleteButton).style.display = "inline";

      // Hide add button
      document.querySelector(uiElements.addButton).style.display = "none";
    },
    updateTotalCalories: function () {
      const totalCalories = ItemCtrl.getItems().reduce(
        (sum, item) => sum + item.calories,
        0
      );
      document.querySelector(
        uiElements.totalCaloriesLabel
      ).textContent = `${totalCalories}`;
    },
  };
})(ItemCtrl);

//Main Controller
const MainCtrl = (function (ItemCtrl, StorageCtrl, UiCtrl) {
  // Private data

  // Load event listeners
  const loadEventListeners = function () {
    // Get UI selectors
    const uiElements = UiCtrl.getUiSelectors();

    //Add button
    document
      .querySelector(uiElements.addButton)
      .addEventListener("click", addNewItem);

    // Clear button
    document
      .querySelector(uiElements.clearBtn)
      .addEventListener("click", clearItems);

    //   Disable enter
    document.addEventListener("keypress", (e) => {
      if (e.keyCode === 13 || e.which === 13) {
        e.preventDefault();
        return false;
      }
    });

    // update button
    document
      .querySelector(uiElements.updateButton)
      .addEventListener("click", updateMenuItem);

    // edit menu item event
    document
      .querySelector(uiElements.itemList)
      .addEventListener("click", updateMenuItem);

    // menu item update event - confirmation
    document
      .querySelector(uiElements.cardContainer)
      .addEventListener("click", confirmUpdate);

    // back button
    document
      .querySelector(uiElements.backButton)
      .addEventListener("click", () => {
        UiCtrl.clearEditState();
      });

    // delete button
    document
      .querySelector(uiElements.deleteButton)
      .addEventListener("click", deleteMenuItem);
  };

  // Event handlers

  //Add new item
  const addNewItem = function (e) {
    e.preventDefault();

    // Get item input
    const newItemInput = UiCtrl.getItemInput();

    // Validate input
    if (inputValid(newItemInput)) {
      const newItem = ItemCtrl.createNewItem(newItemInput);

      // Add item to storage
      StorageCtrl.saveItemToStorage(newItem);

      //  Add item to UI
      UiCtrl.renderSingleItemToUI(newItem);

      UiCtrl.updateTotalCalories();

      //  Clear input values
      UiCtrl.clearFields();
    }
  };

  const clearItems = function (event) {
    event.preventDefault();
    // Clear UI items
    document.querySelector(UiCtrl.getUiSelectors().itemList).innerHTML = "";

    // Clear items from LS
    let storageItems = StorageCtrl.getItemsListFromStorage();
    storageItems = [];

    // save local storage
    localStorage.setItem("menuItems", JSON.stringify(storageItems));
  };

  // delete menu item
  const deleteMenuItem = function (event) {
    // get current menu item id
    const currentMealId = ItemCtrl.getCurrentMeal().id;

    // delete menu item in array
    const updatedItemList = ItemCtrl.getItems().filter(
      (item) => item.id != currentMealId
    );

    //update array
    ItemCtrl.updateItemList(updatedItemList);

    // clear menu item list
    document.querySelector(UiCtrl.getUiSelectors().itemList).innerHTML = "";

    // update calories count
    UiCtrl.updateTotalCalories();

    // Clear input
    UiCtrl.clearFields();

    UiCtrl.clearEditState();

    // re-populate menu item list
    updatedItemList.forEach(UiCtrl.renderSingleItemToUI);

    // update LS
    StorageCtrl.removeItemFromStorage(currentMealId);
  };

  // Show menu item to edit in FORM
  const updateMenuItem = function (event) {
    const uiElements = UiCtrl.getUiSelectors();
    event.preventDefault();
    const elementClicked = event.target;

    // Edit button clicked
    if (elementClicked.classList.contains("edit-item")) {
      //get list item id
      const mealName = elementClicked.parentElement.parentElement.id;
      const id = parseInt(mealName.split("-")[1]);

      // get meal item
      const mealToEdit = ItemCtrl.getMealById(id);

      //  set meal item in DS
      ItemCtrl.setCurrentMeal(mealToEdit);

      // show meal name and calories in UI (FORM)
      UiCtrl.showEditMeal(mealToEdit);
    }
  };

  // Confirm update change
  const confirmUpdate = function (event) {
    event.preventDefault();

    // Get UI elements
    const uiElements = UiCtrl.getUiSelectors();

    // Get Menu items
    const menuItems = ItemCtrl.getItems();

    const targetClicked = event.target;

    //Check if update clicked
    if (targetClicked.classList.contains("update-btn")) {
      // get current meal id
      const currentMealId = ItemCtrl.getCurrentMeal().id;

      // get new menu item description
      const updatedName = document.querySelector(uiElements.mealName).value;
      const updatedCalories = parseInt(
        document.querySelector(uiElements.mealCalories).value
      );

      // Change current meal with new name and calorie - DS
      ItemCtrl.changeMealInfo(currentMealId, updatedName, updatedCalories);

      // get updated meal
      const updatedMeal = ItemCtrl.getMealById(currentMealId);
      // Update meal info in LS
      StorageCtrl.updateItemList(updatedMeal);

      //   Clear current meal list
      UiCtrl.clearMealList();

      //update UI
      menuItems.forEach((item) => UiCtrl.renderSingleItemToUI(item));

      // Update calories
      UiCtrl.updateTotalCalories();

      // Clear edit state
      UiCtrl.clearEditState();
    } else if (targetClicked.classList.contains("delete-btn")) {
      // delete menu item
    }

    // Check if delete button clicked
  };

  const inputValid = function (newItem) {
    if (newItem.mealName !== "" && newItem.mealCalories !== "") {
      return true;
    }
    return false;
  };

  // Public properties/functions

  // Initialize App
  return {
    init: function () {
      // clear edit state
      UiCtrl.clearEditState();

      //Fetch meals from data structure
      const items = ItemCtrl.getItems();

      // Hide meal list if empty; otherwise populate meals
      if (items.length === 0) {
        UiCtrl.hideMealList();
      } else {
        // render items to UI
        UiCtrl.renderItemsToUI(items);
      }

      // Show total calories
      UiCtrl.updateTotalCalories();

      loadEventListeners();
    },
  };
})(ItemCtrl, StorageCtrl, UiCtrl);

// Initialize app
MainCtrl.init();
